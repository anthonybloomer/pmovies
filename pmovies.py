#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from tmdbv3api import TMDb
import urllib2
import tweepy
import schedule
import time
import config
from os.path import expanduser
import urllib
import os
import sys

pictures_path = expanduser(config.pictures_path)

auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
auth.set_access_token(config.access_key, config.access_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

tmdb = TMDb(api_key=config.tmdb_api_key)

URL = 'http://pmovies.to/movies/'
opener = urllib2.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]


def bot():
    response = opener.open(URL)
    soup = BeautifulSoup(response, 'html.parser')
    items = soup.find_all('div', {'class': 'ml-item'})

    for item in items:
        a = item.find('a', href=True)

        url = a['href']
        title = a['title']
        search = tmdb.search(title)

        try:
            first = search[0]
            title = first.title
            lines = open('movies.txt', 'r+')
            titles = lines.read()

            if title not in titles:
                print "Doesn't exit. Posting to Twitter."
                lines.write(title + '\n')
                if first.poster_path:
                    movie = tmdb.get_movie(first.id)
                    genre = movie.genres[0]['name']
                    image_path = pictures_path + first.poster_path
                    urllib.urlretrieve(config.tmdb_base_image_path + first.poster_path, image_path)
                    release = first.release_date
                    year = release.split('-')[0]
                    status = '%s (%s) - %s #movies #films #%s' % (title, year, url, genre.lower())
                    api.update_with_media(image_path, status=status)
                    os.remove(image_path)
                    print('Status updated: ' + status)

            else:
                print "Exists."
        except IndexError:
            pass


def main():
    bot()
    schedule.every(60).minutes.do(bot)
    while 1:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit()
